using ChatDB;
using Discord;
using Discord.WebSocket;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.SignalR;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SignalRChat.Hubs
{
    class User
    {
        public string email;
        public string token;
        public bool authneticated = false;
        Regex rgx = new Regex("[^a-zA-Z0-9]");

        public User(string email, string token)
        {
            this.email = email;
            this.token = token;
        }

        public string ValidUsername()
        {
            return rgx.Replace(email.Split("@")[0], "");
        }
    }

    public class ChatHub : Hub
    {
        private DiscordSocketClient discordBot;
        private Dictionary<string, User> userLookup = new Dictionary<string, User>();
        private string authEmail;
        private string authPassword;
        private SocketGuild socketGuild;

        public ChatHub(DiscordSocketClient discordBot, ChatDBContext chatDBContext)
        {
            this.discordBot = discordBot;
            this.discordBot.MessageReceived += this.DiscordMessageReceived;
            this.authEmail = Environment.GetEnvironmentVariable("EMAIL");
            this.authPassword = Environment.GetEnvironmentVariable("PASSWORD");
            this.discordBot.JoinedGuild += this.JoinedGuild;
        }

        private Task JoinedGuild(SocketGuild socketGuild)
        {
            this.socketGuild = socketGuild;
            return Task.CompletedTask;
        }

        private async Task DiscordMessageReceived(SocketMessage socketMessage)
        {
            //socketMessage.Channel.
        }

        private void SendEmail(string email)
        {
            if (userLookup.ContainsKey(Context.ConnectionId))
            {
                try
                {
                    var mailMessage = new MimeMessage();
                    mailMessage.From.Add(new MailboxAddress("Anton", "antonblomstrom97@gmail.com"));
                    var name = email.Split('@')[0];
                    mailMessage.To.Add(new MailboxAddress(name, email));
                    mailMessage.Subject = "Login to AntBlo Chat";

                    var token = Guid.NewGuid().ToString();
                    userLookup.Add(Context.ConnectionId, new User(email, token));
                    mailMessage.Body = new TextPart("plain")
                    {
                        Text = $"Copy into token field: {token}"
                    };
                    using (var smtpClient = new SmtpClient())
                    {
                        smtpClient.Connect("smtp.gmail.com", 465, true);
                        var authEmail = this.authEmail;
                        var authPassword = this.authPassword;
                        smtpClient.Authenticate(authEmail, authPassword);
                        smtpClient.Send(mailMessage);
                        smtpClient.Disconnect(true);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        private void SendToken(string token)
        {
            User user = userLookup[Context.ConnectionId];
            if (user.token.Equals(token))
            {
                user.authneticated = true;
            }
        }

        private async Task SendMessage(string message)
        {
            User user = userLookup[Context.ConnectionId];
            if (user.authneticated)
            {
                var restTextChannel = await socketGuild.CreateTextChannelAsync(user.ValidUsername());
            }
        }

        override public Task OnDisconnectedAsync(Exception? exception)
        {
            if (exception != null)
            {
                Console.WriteLine(exception);
            }
            userLookup.Remove(Context.ConnectionId);
            return Task.CompletedTask;
        }
    }
}
