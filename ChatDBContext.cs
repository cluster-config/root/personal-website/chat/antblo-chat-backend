using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace ChatDB
{
    public class ChatDBContext : DbContext
    {
        public ChatDBContext()
        {
            this.Database.Migrate();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var postgres = Environment.GetEnvironmentVariable("POSTGRES");
            optionsBuilder.UseNpgsql($"Host=postgres-postgresql.postgres.svc.cluster.local;Port=5432;Database=antblo-chat;Username=postgres;Password={postgres}");
        }
    }

    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; set; }
    }

    public class Post
    {
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}
